import java.util.ArrayList;
import java.util.Scanner;

class Worker {
    int id;
    String name;
    String gender;
    float wage; //
    float hours;

    Worker() {
    }

    Worker(int id, String name, String gender, float wage, float hours) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.wage = wage;
        this.hours = hours;
    }

    float findSalary() {
        return hours * wage;
    }

    void outputInformation() {
        System.out.println("---------------------------------");
        System.out.println("ID : " + id);
        System.out.println("Name : " + name);
        System.out.println("Gender :" + gender);
        System.out.println("Salary : " + findSalary() + "$");
    }

    void inputInformation(Scanner input) {
        boolean isIDValid = false;
        do {
            String strId;
            System.out.println("Enter ID : ");
            try {
                strId = input.next();
                id = Integer.parseInt(strId);
                isIDValid = true;
            } catch (Exception ex) {
                System.out.println(" Invalid ID format!! (Integer only! )");
            }
        } while (!isIDValid);

        System.out.println("Enter Name : ");
        input.nextLine();
        name = input.nextLine();
        System.out.println("Enter Gender : ");
        gender = input.nextLine();
        System.out.println("Enter hours : ");
        hours = input.nextFloat();
        System.out.println("Enter wage : ");
        wage = input.nextFloat();

    }

}

public class Main {
    static void pressEnterKey(){
        Scanner scanner = new Scanner(System.in);
        System.out.println(" ========================Press Enter to continue========================");
        scanner.nextLine();
    }
    public static void main(String[] args) {
        ArrayList<Worker> workers = new ArrayList<>();
        Scanner input = new Scanner(System.in);
        int option = 0;
        do {
            System.out.println("--------------- Worker System ------------------");
            System.out.println("1. Add new worker ");
            System.out.println("2. Edit worker information ");
            System.out.println("3. Delete worker profile");
            System.out.println("4. Search worker profile");
            System.out.println("5. Show worker information ");
            System.out.println("6. Exit");

            String strOption;
            System.out.print(">>> Choose option from 1 - 5 : ");
            strOption = input.nextLine();
            try {
                option = Integer.parseInt(strOption);
            } catch (Exception ex) {
                option = 0;
                System.out.println("Invalid Input !!!");
            }


            switch (option) {
                case 1:
                    System.out.println("------------- Add New Worker------------- ");
                    Worker newWorker = new Worker();
                    newWorker.inputInformation(input);
                    workers.add(newWorker);
                    System.out.println("Successfully added.....!");
                    input.nextLine();
                    break;
                case 2 :
                    System.out.println("Edit Informatioin"); break;

                case 4 :
                    int searchOption;
                    System.out.println("************ Search Worker***********");
                    System.out.println("1. Search By ID ");
                    System.out.println("2. Search By Name ");
                    System.out.println("3. Exit....");

                    System.out.print("Choose option 1 - 3 : ");
                    searchOption = input.nextInt();
                    switch(searchOption){
                        case 1 :
                            int searchID;
                            boolean isWorkerExist = false;
                            System.out.println("*********** Search By ID *********** ");
                            System.out.println("Enter ID to search : ");
                            searchID = input.nextInt();

                            for(int i = 0; i < workers.size(); i++){
                                if(searchID== workers.get(i).id){
                                    // worker exist
                                    isWorkerExist = true;
                                    workers.get(i).outputInformation();
                                }
                            }
                            if(!isWorkerExist){
                                System.out.println("Worker with ID = "+searchID+" doesn't exist !!!");
                            }
                            pressEnterKey();
                            break;
                        case 2 :
                            System.out.println("*********** Search By Name *********** ");
                            String searchName ;
                            System.out.println("Enter name to search : ");
                            searchName = input.nextLine();

                            for(int i =0; i < workers.size(); i++){
                                if(searchName.equals(workers.get(i).name)){
                                    // user exist
                                }
                            }
                    }
                    input.nextLine();
                    break;
                case 5:
                    System.out.println("------------- Show worker information ------------- ");
                    for (int i = 0; i < workers.size(); i++) {
                        workers.get(i).outputInformation();
                    }
                    break;

                case 6:
                    System.out.println("Exit the program...!!");
                    break;
//                default:
//                    System.out.println("Wrong option !! ");  break;
            }
        } while (option != 6);

    }
}